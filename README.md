# YoungBot
Multi-platform chat bot that does everything from giving you the best music, to delivering the weather for you in the morning, and more.
 
Created by [youngchief](https://ycws.vercel.app) and others. 
Used in various chatrooms. 
Powered by [Megabridge](https://github.com/youngchief-btw/Megabridge) & your [donations](https://ycws.vercel.app/support.html).

To-Do:
- [ ] [Pronouns](To-Do/Pronouns.md)
- [ ] [Weather](To-Do/Weather.md)
- [ ] [Music](To-Do/Music.md)
- [ ] [Hacker News](To-Do/HackerNews.md)
- [ ] [Lobsters](To-Do/Lobsters.md)
- [ ] [AbuseIPDB](To-Do/AbuseIPDB.md)
- [ ] [Ping/Pong](To-Do/PingPong.md)
- [ ] [Recording](To-Do/Recording.md)
- [ ] [Streaming](To-Do/Streaming.md)
- [ ] [Birthdays](To-Do/Birthdays.md)
- [ ] [Reminders](To-Do/Reminders.md)
- [ ] [Timers]((To-Do/Timers.md))
- [ ] [Help](To-Do/Help.md)
- [ ] [Support](To-Do/Support.md)
- [ ] [URL shortening](To-Do/URLShortening.md)
- [ ] [File sharing/hosting](To-Do/FileSharingSlashHosting.md)
- [ ] [PGP](To-Do/PGP.md)
- [ ] [Git](To-Do/Git.md)
- [ ] [Reddit](To-Do/Reddit.md)
- [ ] [Fediverse](To-Do/Fediverse.md)
- [ ] [Social Media](To-Do/SocialMedia.md)
- [ ] [Moderation](To-Do/Moderation.md)
- [ ] [Soundboard](To-Do/Soundboard.md)
- [ ] [Text-to-speech](To-Do/TextToSpeech.md)
- [ ] [Speech-to-text](To-Do/SpeechToText.md)
- [ ] [Games](To-Do/Games.md)
- [ ] [WHOIS](To-Do/WHOIS.md)
- [ ] [YoungChief Accounts](To-Do/YoungChiefAccounts.md)

